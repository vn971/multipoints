addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.4.0") // eclipse with-source=true

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.6.0") // gen-idea


addSbtPlugin("io.spray" % "sbt-revolver" % "0.7.1") // ~re-start


addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2") // assembly
